#ifndef HistNameSvc_h
#define HistNameSvc_h

#include <string>
#include <map>

class HistNameSvc {

public:

protected:

  // buffer for the full output name
  std::string m_name;
  std::string m_sample;
  int m_nVRJet;
  std::string m_leptonFlav;
  std::string m_collectionPeriod;
  std::string m_description;
  int m_tagged; //-1 means not apply tagging (all)
  std::string m_truthLabel;
  std::string m_variation;
  bool m_isNominal;
  bool m_doOneSysDir;
  
  //values for truth studies
  bool m_doTruthStudies;
  bool m_passChi2;
  bool m_fullMatch;
  bool m_bMatch;
  bool m_isB;

public:

  HistNameSvc();
  ~HistNameSvc() {};

  std::map<int, std::string> m_samples;

  virtual void reset(bool resetSample = false);

  virtual std::string getFullHistName(const std::string& variable);

  std::string getEventFlavour();
  std::string getFullSample();

  std::string get_leptonFlav(){ return m_leptonFlav; }
  void set_leptonFlav(const std::string & leptonFlav){m_leptonFlav = leptonFlav;}

  std::string get_collectionPeriod(){ return m_collectionPeriod;}
  void set_collectionPeriod(const std::string &collectionPeriod){m_collectionPeriod = collectionPeriod;}

  void set_isNominal();
  bool get_isNominal() { return m_isNominal; }

  virtual void set_sample(const std::string& sample);
  std::string get_sample(){ return m_sample;  }

  void set_nVRJet(int nVRJet) { m_nVRJet = nVRJet; }
  int get_nVRJet() { return m_nVRJet; }

  void set_tagged(int tagged) { m_tagged = tagged; }
  int get_tagged() { return m_tagged; }

  void set_truthLabel(const std::string& truthLabel) { m_truthLabel = truthLabel; }
  std::string get_truthLabel() { return m_truthLabel; }

  void set_description(const std::string& description) { m_description = description; }
  void set_variation(const std::string& variation);

  //needed for truth studies
  void set_doTruthStudies(bool doTruthStudies){ m_doTruthStudies = doTruthStudies; }
  void set_passChi2(bool passChi2){ m_passChi2 = passChi2; }
  void set_fullMatch(bool fullMatch){ m_fullMatch = fullMatch; }
  void set_bMatch(bool bMatch){ m_bMatch = bMatch; }
  void set_isB(bool isB){ m_isB = isB; }

  std::string get_description() { return m_description; }

  void set_doOneSysDir(bool doOneSysDir);

};

#endif
