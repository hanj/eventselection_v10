#include <EventLoop/DirectDriver.h>
#include <EventLoop/CondorDriver.h>
#include <EventLoop/Job.h>
#include <TSystem.h>
#include <SampleHandler/ScanDir.h>
#include <SampleHandler/ToolsDiscovery.h>

void resubmit_jobs (const std::string& submitDir)
{
  // Resubmit failed jobs (all must have finished running)
  EL::Driver::resubmit (submitDir, "ALL_MISSING");
}
