from ROOT import *
import argparse
import os

##
## take input args
##
parser = argparse.ArgumentParser(description='python wrapper to merge the outputs from the EventSelection step')
parser.add_argument('-i','--input_folder', help='Folder containing the input files resulting from the "retrieve_jobs.cxx" script', required=True, dest='input_folder')
parser.add_argument('-t','--ttbar_modelling', help='Comma-separated list of ttbar modelling uncertainties', default="none", dest='ttbar_modelling')
parser.add_argument('-o','--output_file', help='Name of the output file', default="total.root", dest='output_file')
args = parser.parse_args()
input_folder = args.input_folder
ttbar_modelling = args.ttbar_modelling
output_file = args.output_file

##
## ttbar samples to consider
##
ttbar_samples = ["ttbar_PP8"]
if not ttbar_modelling is "none":
	for sample in ttbar_modelling.split(","): ttbar_samples+=[sample]

##
## other samples to consider
##
Wjets = ["Wenu","Wmunu","Wtaunu"]
Zjets = ["Zee","Zmumu","Ztautau"]
Small = ["diboson","single_top","ttV"]
Data = ["data*"]
other_samples = Wjets + Zjets + Small + Data

##
## creating the outputs
##
for ttbar in ttbar_samples:
	ofile = args.output_file.replace(".root","_"+ttbar+".root")
	command = "hadd "+ofile+" "
	command += input_folder+"/hist-"+ttbar+".root "
	for other in other_samples:
		command += input_folder+"/hist-"+other+".root "
	os.system(command)