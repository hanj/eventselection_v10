//---------------------------------------------------------------
// Event selection steering macro: 
// for the b-tagging calibration using semileptonic ttbar events
// (following the ASG design guidelines)
//---------------------------------------------------------------

//BM: missing includes -> put in there for standalone execution

//asg inlcudes
#include "AsgTools/MessageCheck.h"

//sample handler includes
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/Sample.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/DiskListLocal.h"

//event loop includes
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoop/TorqueDriver.h"
#include "EventLoop/LSFDriver.h"
#include "EventLoop/CondorDriver.h"

//cpp includes
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <vector>

//algorithm inlcude
#include "EventSelection/EventSelection.h"
#include "EventSelection/ConfigStore.h"


//ROOT includes
#include <TSystem.h>

int main(int argc, char* argv[]) {

  using namespace asg::msgUserCode;

  std::cout << "-------------------------------------" << std::endl;
  std::cout << "| SL ttbar T&P event selection tool |" << std::endl;
  std::cout << "-------------------------------------" << std::endl;

  //BM: Now try to read in the most important steering tasks from a config file (many thanks to the CxAODFramework guys from which I have stolen most of the code)
  std::string submitDir = "submitDir";
  std::string configPath = "data/EventSelection/selectEvents.cfg"; //figure out what to put here
  if (argc > 1) submitDir = argv[1];
  if (argc > 2) configPath = argv[2];

  // read config
  ConfigStore* config = ConfigStore::createStore(configPath);

  // create a new sample handler to describe the data files we use
  SH::SampleHandler sh;

  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
  std::string dataset_dir = config->get<std::string>("dataset_dir");

  std::vector<std::string> sample_names;
  config->getif<std::vector<std::string> >("samples", sample_names);

  //first check whether the samples wanted exist or not
  Info("runEventSelection()","Scanning the provided samples:");
  for(unsigned int isamp(0); isamp<sample_names.size(); isamp++){
    std::string sample_name(sample_names.at(isamp));
    std::string sample_dir(dataset_dir+"/"+sample_name);
  
    bool direxists=gSystem->OpenDirectory(sample_dir.c_str());
    if(!direxists){
      Warning("runEventSelection()","The following file does not exist: %s",sample_dir.c_str());
    }   

    //now load them into sample handler
    SH::DiskListLocal list(sample_dir);
    SH::ScanDir()
      .samplePattern("*") //BM: change pattern here
      .sampleName(sample_name)
      .scan(sh,list);
    //SH::scanSingleDir (sampleHandler, sample_name, list, "*CxAOD*") ;

    SH::Sample* sample_ptr = sh.get(sample_name);
    sample_ptr->meta()->setString("SampleID",sample_name);
    int nsampleFiles = sample_ptr->numFiles();

    Info("runEventSelection()","Sample name %s with nfiles : %d",sample_name.c_str(), nsampleFiles);
  }

  // set the name of the tree in our files
  std::string tree_name = "nominal";
  config->getif<std::string>("tree_name", tree_name);
  sh.setMetaString ("nc_tree", tree_name.c_str());

  // further sample handler configuration may go here
  // BM: atm no further changes here, but maybe crosscheck with CxAODFW

  // print out the samples we found
  Info("runEventSelection()","Printing sample handler contents:");
  sh.print ();

  // limit file size per job (sample-wise)
  int jobSizeLimitMB = -1;
  config->getif<int>("jobSizeLimitMB", jobSizeLimitMB);
  if (jobSizeLimitMB > 0) {
    long long int jobSizeLimitByte = (((long long int)jobSizeLimitMB) * 1024) * 1024;
    std::cout<< "Limiting file size per job (sample-wise) to " << jobSizeLimitMB << " MB" << std::endl;
    SH::SampleHandler::iterator it = sh.begin();
    SH::SampleHandler::iterator it_end = sh.end();
    // loop over samples
    for (; it != it_end; it++) {
      SH::Sample* sample = *it;
      std::vector<long long int> fileSizes;
      // loop over files
      unsigned int nFiles = sample->numFiles();
      for (unsigned int i = 0; i < nFiles; i++) {
        TString fileName = sample->fileName(i);
        // Depending on wether the file is located on eos or locally, the script will act differently.
        if (fileName.BeginsWith("file:///")) {
          fileName.ReplaceAll("file:///", "/");
        }
        else if (fileName.BeginsWith("root://eosatlas.cern.ch://")) {
          fileName.ReplaceAll("root://eosatlas.cern.ch://", "/");
        }
        else continue;
        // open file with cursor at the end:
	std::ifstream fileStream(fileName.Data(), std::ios::binary | std::ios::ate);
        long long int fileBytes = fileStream.tellg();
	fileSizes.push_back(fileBytes);
      }

      // go through list of sizes and estimate maximal nFilesPerJob
      unsigned int nFilesPerJob = 0;
      bool jobsAreSmaller = true;
      while (jobsAreSmaller) {
        nFilesPerJob++;
        long long int jobSize = 0;
        unsigned int filePerJobCounter = 0;
        for (unsigned int i = 0; i < fileSizes.size(); i++) {
          jobSize += fileSizes.at(i);
          filePerJobCounter++;
          if (jobSize > jobSizeLimitByte) {
            jobsAreSmaller = false;
            break;
          }
          if (filePerJobCounter > nFilesPerJob) {
            jobSize = 0;
            filePerJobCounter = 0;
          }
        }
	if (nFilesPerJob > nFiles) {
          break;
        }
      }

      // even out nFilesPerJob
      if (nFilesPerJob >= nFiles) {
        nFilesPerJob = nFiles;
      }else if (nFilesPerJob > nFiles/2 + 1) {
        nFilesPerJob = nFiles/2 + 1;
      }
      // set number of files per job
      std::cout << "Sample " << sample->name()
                << ": nFiles = " << nFiles
                << ", nFilesPerJob = " << nFilesPerJob << std::endl;
      sample -> meta() -> setDouble(EL::Job::optFilesPerWorker, nFilesPerJob);
    }
  }


  // this is the basic description of our job
  EL::Job job;
  job.sampleHandler (sh); // use SampleHandler in this job

  //limit number of events to maxEvents - set in config
  job.options()->setDouble (EL::Job::optMaxEvents, config->get<int>("maxEvents"));

  // add our algorithm to the job
  // BM: maybe even change this to read in from a config file
  EventSelection* algorithm = new EventSelection();

  //set the config file to be used in the EventSelection class
  algorithm->setConfig(config);

  // later on we'll add some configuration options for our algorithm that go here
  job.algsAdd(algorithm);

  // Number of files to submit per job
  int nFilesPerJob = 20;
  config->getif<int>("nFilesPerJob", nFilesPerJob);

  // make the driver we want to use:
  std::string driver = "direct";
  config->getif<std::string>("driver",driver);

  if (driver=="direct"){
    EL::DirectDriver*  eldriver = new EL::DirectDriver;
    eldriver->submit(job, submitDir);
  }else if (driver=="torque"){
    EL::TorqueDriver* eldriver = new EL::TorqueDriver;
    eldriver->shellInit = "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase && source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh";
    std::string stbc_queue = "generic";
    config->getif<std::string>("stbc_queue",stbc_queue);
    job.options()->setDouble (EL::Job::optFilesPerWorker, nFilesPerJob);
    job.options()->setString (EL::Job::optSubmitFlags, ("-q "+ stbc_queue).c_str()); //generic queue default - 24h walltime
    eldriver -> submitOnly(job,submitDir);
  }else if(driver=="LSF"){
    EL::LSFDriver* eldriver = new EL::LSFDriver;
    eldriver->options()->setString (EL::Job::optSubmitFlags, "-L /bin/bash");
    eldriver->shellInit = "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase && source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh";
    job.options()->setDouble (EL::Job::optFilesPerWorker, nFilesPerJob);
    std::string bQueue = "1nh";
    config->getif<std::string>("bQueue",bQueue);
    job.options()->setString (EL::Job::optSubmitFlags, ("-q "+bQueue).c_str()); //1nh 8nm
    eldriver->submitOnly(job, submitDir);
  }else if(driver == "condor"){
    EL::CondorDriver* eldriver = new EL::CondorDriver;
    eldriver->shellInit = "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase && source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh";
    job.options()->setDouble (EL::Job::optFilesPerWorker, nFilesPerJob);
    job.options()->setString (EL::Job::optCondorConf, "+JobFlavour = \"longlunch\"");
    //job.options()->setString (EL::Job::optCondorConf, "+RequestMemory = 5G");
    eldriver->submitOnly(job, submitDir);
  }else{
    Error("hsg5framework", "Unknown driver '%s'", driver.c_str());
    return 0;
  }
  return 0;
}
