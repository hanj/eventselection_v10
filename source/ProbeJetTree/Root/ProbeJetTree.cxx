#include "ProbeJetTree/ProbeJetTree.h"

static void CreateBranch(TTree* tree, const char* name, void* var, const char* type)
{
    tree->Branch(name, var, type);
}

static void ConnectBranch(TTree* tree, const char* name, void* var, const char*)
{
	tree->SetBranchAddress(name, var);
}

void ProbeJetTree::CreateTree(const char* name)
{
	tree = new TTree(name, name);
	ForEachBranch(CreateBranch);
	// tree->Branch("hadName", &hadName); //disable hadname now since conflict with root_pandas
}

void ProbeJetTree::ConnectTree(TTree* probjeJetTree)
{
	tree = probjeJetTree;
	ForEachBranch(ConnectBranch);
	// tree->SetBranchAddress("hadName", &hadNamePtr); //L: really work?? Now akip hadname branch...
}

