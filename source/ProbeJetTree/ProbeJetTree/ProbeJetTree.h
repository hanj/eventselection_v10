#ifndef PROBE_JET_TREE_H
#define PROBE_JET_TREE_H

#include <functional>
#include <string>
#include <TTree.h>
#include <TString.h>

struct ProbeJetTree
{
	TTree *tree = nullptr;
	//L: now it is small-R jet!

	double ev_weight;
	double xs_weight;
  double ev_weight_pileup_UP;
  double  ev_weight_jvt_UP;
  double ev_weight_leptonSF_EL_SF_Trigger_UP;
  double ev_weight_leptonSF_EL_SF_Reco_UP;
  double ev_weight_leptonSF_EL_SF_ID_UP;
  double ev_weight_leptonSF_EL_SF_Isol_UP;
  double ev_weight_leptonSF_MU_SF_Trigger_STAT_UP;
  double ev_weight_leptonSF_MU_SF_Trigger_SYST_UP;
  double ev_weight_leptonSF_MU_SF_ID_STAT_UP;
  double ev_weight_leptonSF_MU_SF_ID_SYST_UP;
  double ev_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;
  double ev_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;
  double ev_weight_leptonSF_MU_SF_Isol_STAT_UP;
  double ev_weight_leptonSF_MU_SF_Isol_SYST_UP;
  double ev_weight_leptonSF_MU_SF_TTVA_STAT_UP;
  double ev_weight_leptonSF_MU_SF_TTVA_SYST_UP;

  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_1_up;
  float weight_trk1_btag_70WP;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_2_up;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_3_up;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_4_up;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_5_up;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_6_up;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_7_up;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_8_up;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_9_up;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_1_up;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_2_up;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_3_up;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_4_up;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_1_up;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_2_up;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_3_up;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_4_up;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_extrapolation_up;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_extrapolation_from_charm_up;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_1_down;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_2_down;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_3_down;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_4_down;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_5_down;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_6_down;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_7_down;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_8_down;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_9_down;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_1_down;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_2_down;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_3_down;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_4_down;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_1_down;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_2_down;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_3_down;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_4_down;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_extrapolation_down;
  float ev_weight_perjet_trackjet1_bTagSF_DL1r_70_extrapolation_from_charm_down;



  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_1_up;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_2_up;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_3_up;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_4_up;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_5_up;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_6_up;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_7_up;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_8_up;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_9_up;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_1_up;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_2_up;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_3_up;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_4_up;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_1_up;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_2_up;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_3_up;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_4_up;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_extrapolation_up;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_extrapolation_from_charm_up;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_1_down;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_2_down;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_3_down;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_4_down;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_5_down;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_6_down;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_7_down;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_8_down;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_9_down;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_1_down;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_2_down;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_3_down;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_4_down;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_1_down;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_2_down;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_3_down;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_4_down;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_extrapolation_down;
  float ev_weight_perjet_trackjet2_bTagSF_DL1r_70_extrapolation_from_charm_down;



  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_1_up;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_2_up;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_3_up;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_4_up;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_5_up;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_6_up;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_7_up;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_8_up;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_9_up;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_1_up;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_2_up;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_3_up;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_4_up;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_1_up;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_2_up;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_3_up;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_4_up;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_extrapolation_up;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_extrapolation_from_charm_up;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_1_down;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_2_down;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_3_down;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_4_down;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_5_down;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_6_down;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_7_down;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_8_down;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_9_down;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_1_down;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_2_down;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_3_down;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_4_down;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_1_down;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_2_down;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_3_down;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_4_down;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_extrapolation_down;
  float ev_weight_perjet_trackjet3_bTagSF_DL1r_70_extrapolation_from_charm_down;

  double ev_weight_pileup_DOWN;
  double ev_weight_jvt_DOWN;
  double ev_weight_leptonSF_EL_SF_Trigger_DOWN;
  double ev_weight_leptonSF_EL_SF_Reco_DOWN;
  double ev_weight_leptonSF_EL_SF_ID_DOWN;
  double ev_weight_leptonSF_EL_SF_Isol_DOWN;
  double ev_weight_leptonSF_MU_SF_Trigger_STAT_DOWN;
  double ev_weight_leptonSF_MU_SF_Trigger_SYST_DOWN;
  double ev_weight_leptonSF_MU_SF_ID_STAT_DOWN;
  double ev_weight_leptonSF_MU_SF_ID_SYST_DOWN;
  double ev_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;
  double ev_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;
  double ev_weight_leptonSF_MU_SF_Isol_STAT_DOWN;
  double ev_weight_leptonSF_MU_SF_Isol_SYST_DOWN;
  double ev_weight_leptonSF_MU_SF_TTVA_STAT_DOWN;
  double ev_weight_leptonSF_MU_SF_TTVA_SYST_DOWN;


	// float chi2; //not used now
	// float dRmin;
	// float dRVtxJet; //not used now

	float pt;
	float eta;
	float phi;
	float m;
	int label;

	float tagJet_pt;
	float tagJet_eta;
	float tagJet_phi;
	float tagJet_m;
	int tagJet_label;

	float lep_pt;
	float lep_eta;
	float lep_phi;
	int lep_type; //el or mu type
	int CollectionYear; //el or mu type
	int lep_true; //truth type

	float met_met;
	float met_phi;
	



	double mlj;
	double mtw;
	int lepq;
	// double mtop;// see m
	// double m_lVR; //calculate later
	// double dR_l_VR;
	// double dPhi_l_VR;
	// double dR_l_ljet;

	// float mv2c10; //useless now

	//L: added mine
	float DXbb_Top;
	float DXbb_Higgs;
	float DXbb_QCD;
	float DXbb_Top_v3;
	float DXbb_Higgs_v3;
	float DXbb_QCD_v3;
	//float ljet_pt; //obslete, move to pT
	//float ljet_m;
        float probe_pt;
        float probe_eta;
        float probe_phi;
        float probe_m;
        float tag_pt;
        float tag_eta;
        float tag_phi;
        float tag_m;
        float tag_mlj;

	int ljet_label;

	int mcChannelNumber;
	int n_ljet1_VRjet; //leading large-R jet only! no truth information for other large-R jets now in current ntuples.

	int nVHbbTagged;
	int ljet1_label;
	int ljet2_label;
	int ljet3_label;
	int ljet1_VR1_label;
	int ljet1_VR2_label;
	int ljet1_VR3_label;
	int ljet1_VR4_label;
	int ljet1_VR1_labelExt;
	int ljet1_VR2_labelExt;
	int ljet1_VR3_labelExt;
	int ljet1_VR4_labelExt;

	int jetIndex_boosted_top;
	int jetIndex_boosted_b_lep;
        int nLep;
      
        float tagJet_isTagged77;
        float tagJet_isTagged70;
        float tagJet_isTagged60;
        float tagJet_isTagged85;
	
	int njets;
	int nbjets;
	int nVRjets; //for all event
	int nljets;

	std::string hadName;
	std::string *hadNamePtr;

	int isSignal;
	int sample;

	// float mva_score; //not used now

	int isCorrectMatch; //??

	void CreateTree(const char *name);
	void ConnectTree(TTree *probjeJetTree);

private:
	// Can be used to either create the branches, or call SetBranchAddress
	void ForEachBranch(std::function<void(TTree *, const char *, void *, const char *)> func)
	{
		func(tree, "ev_weight", &ev_weight, "ev_weight/D");
		func(tree, "xs_weight", &xs_weight, "xs_weight/D");
   func(tree, "ev_weight_pileup_UP", &ev_weight_pileup_UP, "ev_weight_pileup_UP/D");
   func(tree, "ev_weight_jvt_UP", &ev_weight_jvt_UP, "ev_weight_jvt_UP/D");
   func(tree, "ev_weight_leptonSF_EL_SF_Trigger_UP", &ev_weight_leptonSF_EL_SF_Trigger_UP, "ev_weight_leptonSF_EL_SF_Trigger_UP/D");
   func(tree, "ev_weight_leptonSF_EL_SF_Reco_UP", &ev_weight_leptonSF_EL_SF_Reco_UP, "ev_weight_leptonSF_EL_SF_Reco_UP/D");
   func(tree, "ev_weight_leptonSF_EL_SF_ID_UP", &ev_weight_leptonSF_EL_SF_ID_UP, "ev_weight_leptonSF_EL_SF_ID_UP/D");
   func(tree, "ev_weight_leptonSF_EL_SF_Isol_UP", &ev_weight_leptonSF_EL_SF_Isol_UP, "ev_weight_leptonSF_EL_SF_Isol_UP/D");
   func(tree, "ev_weight_leptonSF_MU_SF_Trigger_STAT_UP", &ev_weight_leptonSF_MU_SF_Trigger_STAT_UP, "ev_weight_leptonSF_MU_SF_Trigger_STAT_UP/D");
   func(tree, "ev_weight_leptonSF_MU_SF_Trigger_SYST_UP", &ev_weight_leptonSF_MU_SF_Trigger_SYST_UP, "ev_weight_leptonSF_MU_SF_Trigger_SYST_UP/D");
   func(tree, "ev_weight_leptonSF_MU_SF_ID_STAT_UP", &ev_weight_leptonSF_MU_SF_ID_STAT_UP, "ev_weight_leptonSF_MU_SF_ID_STAT_UP/D");
   func(tree, "ev_weight_leptonSF_MU_SF_ID_SYST_UP", &ev_weight_leptonSF_MU_SF_ID_SYST_UP, "ev_weight_leptonSF_MU_SF_ID_SYST_UP/D");
   func(tree, "ev_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP", &ev_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP, "ev_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP/D");
   func(tree, "ev_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP", &ev_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP, "ev_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP/D");
   func(tree, "ev_weight_leptonSF_MU_SF_Isol_STAT_UP", &ev_weight_leptonSF_MU_SF_Isol_STAT_UP, "ev_weight_leptonSF_MU_SF_Isol_STAT_UP/D");
   func(tree, "ev_weight_leptonSF_MU_SF_Isol_SYST_UP", &ev_weight_leptonSF_MU_SF_Isol_SYST_UP, "ev_weight_leptonSF_MU_SF_Isol_SYST_UP/D");
   func(tree, "ev_weight_leptonSF_MU_SF_TTVA_STAT_UP", &ev_weight_leptonSF_MU_SF_TTVA_STAT_UP, "ev_weight_leptonSF_MU_SF_TTVA_STAT_UP/D");
   func(tree, "ev_weight_leptonSF_MU_SF_TTVA_SYST_UP", &ev_weight_leptonSF_MU_SF_TTVA_SYST_UP, "ev_weight_leptonSF_MU_SF_TTVA_SYST_UP/D");

   func(tree, "weight_trk1_btag_70WP", &weight_trk1_btag_70WP, "weight_trk1_btag_70WP/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_1_up", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_1_up, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_1_up/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_2_up", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_2_up, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_2_up/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_3_up", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_3_up, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_3_up/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_4_up", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_4_up, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_4_up/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_5_up", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_5_up, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_5_up/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_6_up", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_6_up, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_6_up/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_7_up", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_7_up, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_7_up/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_8_up", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_8_up, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_8_up/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_9_up", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_9_up, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_9_up/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_1_up", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_1_up, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_1_up/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_2_up", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_2_up, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_2_up/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_3_up", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_3_up, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_3_up/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_4_up", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_4_up, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_4_up/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_1_up", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_1_up, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_1_up/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_2_up", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_2_up, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_2_up/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_3_up", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_3_up, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_3_up/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_4_up", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_4_up, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_4_up/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_extrapolation_up", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_extrapolation_up, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_extrapolation_up/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_extrapolation_from_charm_up", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_extrapolation_from_charm_up, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_extrapolation_from_charm_up/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_1_down", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_1_down, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_1_down/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_2_down", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_2_down, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_2_down/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_3_down", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_3_down, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_3_down/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_4_down", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_4_down, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_4_down/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_5_down", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_5_down, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_5_down/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_6_down", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_6_down, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_6_down/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_7_down", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_7_down, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_7_down/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_8_down", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_8_down, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_8_down/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_9_down", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_9_down, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_B_9_down/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_1_down", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_1_down, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_1_down/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_2_down", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_2_down, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_2_down/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_3_down", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_3_down, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_3_down/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_4_down", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_4_down, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_C_4_down/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_1_down", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_1_down, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_1_down/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_2_down", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_2_down, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_2_down/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_3_down", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_3_down, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_3_down/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_4_down", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_4_down, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_eigenvars_Light_4_down/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_extrapolation_down", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_extrapolation_down, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_extrapolation_down/F");
   func(tree, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_extrapolation_from_charm_down", &ev_weight_perjet_trackjet1_bTagSF_DL1r_70_extrapolation_from_charm_down, "ev_weight_perjet_trackjet1_bTagSF_DL1r_70_extrapolation_from_charm_down/F");



   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_1_up", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_1_up, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_1_up/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_2_up", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_2_up, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_2_up/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_3_up", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_3_up, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_3_up/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_4_up", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_4_up, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_4_up/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_5_up", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_5_up, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_5_up/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_6_up", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_6_up, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_6_up/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_7_up", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_7_up, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_7_up/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_8_up", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_8_up, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_8_up/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_9_up", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_9_up, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_9_up/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_1_up", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_1_up, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_1_up/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_2_up", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_2_up, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_2_up/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_3_up", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_3_up, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_3_up/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_4_up", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_4_up, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_4_up/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_1_up", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_1_up, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_1_up/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_2_up", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_2_up, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_2_up/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_3_up", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_3_up, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_3_up/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_4_up", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_4_up, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_4_up/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_extrapolation_up", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_extrapolation_up, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_extrapolation_up/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_extrapolation_from_charm_up", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_extrapolation_from_charm_up, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_extrapolation_from_charm_up/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_1_down", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_1_down, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_1_down/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_2_down", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_2_down, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_2_down/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_3_down", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_3_down, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_3_down/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_4_down", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_4_down, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_4_down/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_5_down", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_5_down, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_5_down/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_6_down", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_6_down, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_6_down/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_7_down", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_7_down, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_7_down/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_8_down", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_8_down, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_8_down/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_9_down", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_9_down, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_B_9_down/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_1_down", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_1_down, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_1_down/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_2_down", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_2_down, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_2_down/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_3_down", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_3_down, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_3_down/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_4_down", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_4_down, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_C_4_down/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_1_down", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_1_down, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_1_down/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_2_down", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_2_down, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_2_down/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_3_down", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_3_down, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_3_down/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_4_down", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_4_down, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_eigenvars_Light_4_down/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_extrapolation_down", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_extrapolation_down, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_extrapolation_down/F");
   func(tree, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_extrapolation_from_charm_down", &ev_weight_perjet_trackjet2_bTagSF_DL1r_70_extrapolation_from_charm_down, "ev_weight_perjet_trackjet2_bTagSF_DL1r_70_extrapolation_from_charm_down/F");



   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_1_up", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_1_up, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_1_up/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_2_up", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_2_up, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_2_up/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_3_up", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_3_up, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_3_up/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_4_up", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_4_up, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_4_up/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_5_up", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_5_up, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_5_up/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_6_up", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_6_up, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_6_up/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_7_up", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_7_up, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_7_up/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_8_up", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_8_up, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_8_up/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_9_up", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_9_up, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_9_up/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_1_up", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_1_up, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_1_up/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_2_up", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_2_up, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_2_up/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_3_up", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_3_up, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_3_up/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_4_up", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_4_up, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_4_up/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_1_up", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_1_up, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_1_up/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_2_up", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_2_up, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_2_up/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_3_up", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_3_up, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_3_up/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_4_up", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_4_up, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_4_up/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_extrapolation_up", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_extrapolation_up, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_extrapolation_up/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_extrapolation_from_charm_up", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_extrapolation_from_charm_up, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_extrapolation_from_charm_up/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_1_down", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_1_down, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_1_down/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_2_down", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_2_down, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_2_down/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_3_down", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_3_down, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_3_down/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_4_down", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_4_down, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_4_down/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_5_down", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_5_down, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_5_down/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_6_down", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_6_down, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_6_down/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_7_down", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_7_down, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_7_down/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_8_down", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_8_down, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_8_down/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_9_down", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_9_down, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_B_9_down/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_1_down", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_1_down, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_1_down/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_2_down", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_2_down, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_2_down/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_3_down", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_3_down, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_3_down/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_4_down", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_4_down, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_C_4_down/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_1_down", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_1_down, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_1_down/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_2_down", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_2_down, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_2_down/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_3_down", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_3_down, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_3_down/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_4_down", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_4_down, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_eigenvars_Light_4_down/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_extrapolation_down", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_extrapolation_down, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_extrapolation_down/F");
   func(tree, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_extrapolation_from_charm_down", &ev_weight_perjet_trackjet3_bTagSF_DL1r_70_extrapolation_from_charm_down, "ev_weight_perjet_trackjet3_bTagSF_DL1r_70_extrapolation_from_charm_down/F");


   func(tree, "ev_weight_pileup_DOWN", &ev_weight_pileup_DOWN, "ev_weight_pileup_DOWN/D");
   func(tree, "ev_weight_jvt_DOWN", &ev_weight_jvt_DOWN, "ev_weight_jvt_DOWN/D");
   func(tree, "ev_weight_leptonSF_EL_SF_Trigger_DOWN", &ev_weight_leptonSF_EL_SF_Trigger_DOWN, "ev_weight_leptonSF_EL_SF_Trigger_DOWN/D");
   func(tree, "ev_weight_leptonSF_EL_SF_Reco_DOWN", &ev_weight_leptonSF_EL_SF_Reco_DOWN, "ev_weight_leptonSF_EL_SF_Reco_DOWN/D");
   func(tree, "ev_weight_leptonSF_EL_SF_ID_DOWN", &ev_weight_leptonSF_EL_SF_ID_DOWN, "ev_weight_leptonSF_EL_SF_ID_DOWN/D");
   func(tree, "ev_weight_leptonSF_EL_SF_Isol_DOWN", &ev_weight_leptonSF_EL_SF_Isol_DOWN, "ev_weight_leptonSF_EL_SF_Isol_DOWN/D");
   func(tree, "ev_weight_leptonSF_MU_SF_Trigger_STAT_DOWN", &ev_weight_leptonSF_MU_SF_Trigger_STAT_DOWN, "ev_weight_leptonSF_MU_SF_Trigger_STAT_DOWN/D");
   func(tree, "ev_weight_leptonSF_MU_SF_Trigger_SYST_DOWN", &ev_weight_leptonSF_MU_SF_Trigger_SYST_DOWN, "ev_weight_leptonSF_MU_SF_Trigger_SYST_DOWN/D");
   func(tree, "ev_weight_leptonSF_MU_SF_ID_STAT_DOWN", &ev_weight_leptonSF_MU_SF_ID_STAT_DOWN, "ev_weight_leptonSF_MU_SF_ID_STAT_DOWN/D");
   func(tree, "ev_weight_leptonSF_MU_SF_ID_SYST_DOWN", &ev_weight_leptonSF_MU_SF_ID_SYST_DOWN, "ev_weight_leptonSF_MU_SF_ID_SYST_DOWN/D");
   func(tree, "ev_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN", &ev_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN, "ev_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN/D");
   func(tree, "ev_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN", &ev_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN, "ev_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN/D");
   func(tree, "ev_weight_leptonSF_MU_SF_Isol_STAT_DOWN", &ev_weight_leptonSF_MU_SF_Isol_STAT_DOWN, "ev_weight_leptonSF_MU_SF_Isol_STAT_DOWN/D");
   func(tree, "ev_weight_leptonSF_MU_SF_Isol_SYST_DOWN", &ev_weight_leptonSF_MU_SF_Isol_SYST_DOWN, "ev_weight_leptonSF_MU_SF_Isol_SYST_DOWN/D");
   func(tree, "ev_weight_leptonSF_MU_SF_TTVA_STAT_DOWN", &ev_weight_leptonSF_MU_SF_TTVA_STAT_DOWN, "ev_weight_leptonSF_MU_SF_TTVA_STAT_DOWN/D");
   func(tree, "ev_weight_leptonSF_MU_SF_TTVA_SYST_DOWN", &ev_weight_leptonSF_MU_SF_TTVA_SYST_DOWN, "ev_weight_leptonSF_MU_SF_TTVA_SYST_DOWN/D");



		// func(tree, "chi2", &chi2, "chi2/F");
		// func(tree, "dRmin", &dRmin, "dRmin/F");
		// func(tree, "dRVtxJet", &dRVtxJet, "dRVtxJet/F");

		func(tree, "pt", &pt, "pt/F");
		func(tree, "eta", &eta, "eta/F");
		func(tree, "phi", &phi, "phi/F");
		func(tree, "m", &m, "m/F");
		func(tree, "label", &label, "label/I");

		func(tree, "tagJet_pt", &tagJet_pt, "tagJet_pt/F");
		func(tree, "tagJet_eta", &tagJet_eta, "tagJet_eta/F");
		func(tree, "tagJet_phi", &tagJet_phi, "tagJet_phi/F");
		func(tree, "tagJet_m", &tagJet_m, "tagJet_m/F");
		func(tree, "tagJet_label", &tagJet_label, "tagJet_label/I");
		func(tree, "tagJet_isTagged77", &tagJet_isTagged77,"tagJet_isTagged77/F");
		func(tree, "tagJet_isTagged85", &tagJet_isTagged85,"tagJet_isTagged85/F");
		func(tree, "tagJet_isTagged70", &tagJet_isTagged70,"tagJet_isTagged70/F");
		func(tree, "tagJet_isTagged60", &tagJet_isTagged60,"tagJet_isTagged60/F");

		func(tree, "lep_pt", &lep_pt, "lep_pt/F");
		func(tree, "lep_eta", &lep_eta, "lep_eta/F");
		func(tree, "lep_phi", &lep_phi, "lep_phi/F");
		func(tree, "lep_type", &lep_type, "lep_type/I");
		func(tree, "nLep", &nLep, "nLep/I");

		func(tree, "CollectionYear", &CollectionYear, "CollectionYear/I");
		func(tree, "lep_true", &lep_true, "lep_true/I");

		func(tree, "met_met", &met_met, "met_met/F");
		func(tree, "met_phi", &met_phi, "met_phi/F");
		


		func(tree, "njets", &njets, "njets/I"); //event njet
		func(tree, "nljets", &nljets, "nljets/I");
		func(tree, "nbjets", &nbjets, "nbjets/I");
		func(tree, "nVRjets", &nVRjets, "nVRjets/I"); //event VR jets

		func(tree, "isSignal", &isSignal, "isSignal/I");
		func(tree, "sample", &sample, "sample/I");
		func(tree, "isCorrectMatch", &isCorrectMatch, "isCorrectMatch/I");

		// func(tree, "mv2c10", &mv2c10, "mv2c10/F");
		// func(tree, "mva_score", &mva_score, "mva_score/F");

		//L:
		func(tree, "DXbb_Top", &DXbb_Top, "DXbb_Top/F");
		func(tree, "DXbb_Higgs", &DXbb_Higgs, "DXbb_Higgs/F");
		func(tree, "DXbb_QCD", &DXbb_QCD, "DXbb_QCD/F");
		func(tree, "DXbb_Top_v3", &DXbb_Top_v3, "DXbb_Top_v3/F");
		func(tree, "DXbb_Higgs_v3", &DXbb_Higgs_v3, "DXbb_Higgs_v3/F");
		func(tree, "DXbb_QCD_v3", &DXbb_QCD_v3, "DXbb_QCD_v3/F");
		//func(tree, "ljet_pt", &ljet_pt, "ljet_pt/F");
		//func(tree, "ljet_m", &ljet_m, "ljet_m/F");
		func(tree, "probe_pt", &probe_pt, "probe_pt/F");
		func(tree, "probe_eta", &probe_eta, "probe_eta/F");
		func(tree, "probe_phi", &probe_phi, "probe_phi/F");
		func(tree, "probe_m", &probe_m, "probe_m/F");
		func(tree, "tag_pt", &tag_pt, "tag_pt/F");
		func(tree, "tag_eta", &tag_eta, "tag_eta/F");
		func(tree, "tag_phi", &tag_phi, "tag_phi/F");
		func(tree, "tag_m", &tag_m, "tag_m/F");
		func(tree, "tag_mlj", &tag_mlj, "tag_mlj/F");
		func(tree, "ljet_label", &ljet_label, "ljet_label/I");
		func(tree, "mcChannelNumber", &mcChannelNumber, "mcChannelNumber/I");
		func(tree, "n_ljet1_VRjet", &n_ljet1_VRjet, "n_ljet1_VRjet/I");


		func(tree, "nVHbbTagged", &nVHbbTagged, "nVHbbTagged/I");
		func(tree, "ljet1_label", &ljet1_label, "ljet1_label/I");
		func(tree, "ljet2_label", &ljet2_label, "ljet2_label/I");
		func(tree, "ljet3_label", &ljet3_label, "ljet3_label/I");
		func(tree, "ljet1_VR1_label", &ljet1_VR1_label, "ljet1_VR1_label/I");
		func(tree, "ljet1_VR2_label", &ljet1_VR2_label, "ljet1_VR2_label/I");
		func(tree, "ljet1_VR3_label", &ljet1_VR3_label, "ljet1_VR3_label/I");
		func(tree, "ljet1_VR4_label", &ljet1_VR4_label, "ljet1_VR4_label/I");
		func(tree, "ljet1_VR1_labelExt", &ljet1_VR1_labelExt, "ljet1_VR1_labelExt/I");
		func(tree, "ljet1_VR2_labelExt", &ljet1_VR2_labelExt, "ljet1_VR2_labelExt/I");
		func(tree, "ljet1_VR3_labelExt", &ljet1_VR3_labelExt, "ljet1_VR3_labelExt/I");
		func(tree, "ljet1_VR4_labelExt", &ljet1_VR4_labelExt, "ljet1_VR4_labelExt/I");

		func(tree, "jetIndex_boosted_top", &jetIndex_boosted_top, "jetIndex_boosted_top/I");
		func(tree, "jetIndex_boosted_b_lep", &jetIndex_boosted_b_lep, "jetIndex_boosted_b_lep/I");
		//L: end here

		func(tree, "mlj", &mlj, "mlj/D");
		func(tree, "lepq", &lepq, "lepq/I");
		func(tree, "mtw", &mtw, "mtw/D");
		// func(tree, "mtop", &mtop, "mtop/D");
		// func(tree, "m_lVR", &m_lVR, "m_lVR/D");
		// func(tree, "dR_l_VR", &dR_l_VR, "dR_l_VR/D");
		// func(tree, "dPhi_l_VR", &dPhi_l_VR, "dPhi_l_VR/D");
		// func(tree, "dR_l_ljet", &dR_l_ljet, "dR_l_ljet/D");
	}
};

#endif //PROBE_JET_TREE_H
